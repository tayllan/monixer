defmodule Runner do
  require Logger
  require UrlRepository

  def run do
    Logger.info("Starting the Runner at #{inspect self()}")

    pid = Database.get_connection
    rows = UrlRepository.get_urls_to_ping(pid)

    Logger.info("Got #{length(rows)} URLs to check")

    Enum.each(rows, &spawn_url_checker/1)

    update_with_received_messages(pid, length(rows))
  end

  defp spawn_url_checker(row) do
    parent_pid = self()
    spawn fn() -> UrlChecker.check(parent_pid, row) end
  end

  defp update_with_received_messages(pid, count) do
    case count do
      0 -> :ok
      _ -> update_with_received_message(pid, count - 1)
    end
  end

  defp update_with_received_message(pid, count) do
    receive do
      url ->
        Logger.debug("Received URL with id #{UrlRepository.url(url, :id)} to update at #{inspect self()}")
        UrlRepository.check_url(pid, url)

        update_with_received_messages(pid, count)
    after
      4000 -> :error
    end
  end
end

