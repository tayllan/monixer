defmodule UrlRepository do
  require Record
  require Logger

  Record.defrecord(:url, id: nil, is_ok: false, status: -1, error_message: "")

  def get_urls_to_ping(pid) do
    %Postgrex.Result{rows: rows} = Postgrex.query!(
      pid,
      """
        SELECT id, url, bad_status_codes FROM urls
        WHERE last_checked_with_recurs_on < CURRENT_TIMESTAMP
        ORDER BY id
        LIMIT 10
      """,
      []
    )

    rows
  end

  def check_url(pid, url(id: id, is_ok: is_ok, status: status, error_message: error_message)) do
    if status == -1 do
      Logger.warn("URL of ID #{id} is being updated with error message #{error_message}")
    end

    insert_url_check(pid, id, is_ok, status, error_message)
    update_url_last_checked(pid, id)
  end

  defp insert_url_check(pid, url_id, is_ok, status, error_message) do
    %Postgrex.Result{num_rows: num_rows} = Postgrex.query!(
      pid,
      """
        INSERT INTO url_checks
        (url_id, is_ok, status, error_message)
        VALUES
        ($1, $2, $3, $4)
      """,
      [url_id, is_ok, status, error_message]
    )

    num_rows
  end

  defp update_url_last_checked(pid, id) do
    %Postgrex.Result{num_rows: num_rows} = Postgrex.query!(
      pid,
      """
        UPDATE urls
        SET last_checked_with_recurs_on = CURRENT_TIMESTAMP + (INTERVAL '1 min' * recurs)
        WHERE id = $1
      """,
      [id]
    )

    num_rows
  end
end