defmodule Main do
  require Logger

  def run do
    Logger.info("Starting Monixer application at #{inspect self()}")

    spawn_runner()
  end

  defp spawn_runner() do
    spawn &Runner.run/0

    Process.sleep(5000)
    spawn_runner()
  end
end

