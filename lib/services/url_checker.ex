defmodule UrlChecker do
  require UrlRepository

  def check(parent_pid, [id, url, bad_status_codes]) do
    ping(url)
    |> send_message_to_parent(parent_pid, [id, bad_status_codes])
  end

  defp ping(url) do
    case HTTPoison.head(url, [], [timeout: 4000, recv_timeout: 4000, hackney: [pool: false]]) do
      {:ok, %HTTPoison.Response{status_code: code}} -> {:ok, code}
      {:error, %HTTPoison.Error{reason: err}} ->
        case is_atom(err) do
          true -> {:error, Atom.to_string(err)}
          false -> {:error, "Unknown error"}
        end
    end
  end

  defp send_message_to_parent(ping_status, parent_pid, [id, bad_status_codes]) do
    case ping_status do
      {:ok, code} ->
        is_not_bad_code = is_nil(Enum.find(bad_status_codes, fn(x) -> x == code end))
        url_record = UrlRepository.url(id: id, is_ok: is_not_bad_code, status: code)
        send(parent_pid, url_record)
      {:error, error_message} ->
        url_record = UrlRepository.url(id: id, error_message: error_message)
        send(parent_pid, url_record)
    end
  end
end
