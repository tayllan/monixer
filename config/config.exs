import Config

config :logger, level: :debug

# config :logger,
#   backends: [{LoggerFileBackend, :debug},
#              {LoggerFileBackend, :info},
#              {LoggerFileBackend, :error}]

config :logger, :debug,
  path: "/opt/app/monixer/logs/debug.log",
  level: :debug

config :logger, :info,
  path: "/opt/app/monixer/logs/info.log",
  level: :info

config :logger, :error,
  path: "/opt/app/monixer/logs/error.log",
  level: :error