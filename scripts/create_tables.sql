CREATE TABLE urls (
    id SERIAL PRIMARY KEY,
    url TEXT NOT NULL,
    bad_status_codes INTEGER[] NOT NULL,
    recurs INTEGER NOT NULL,
    last_checked_with_recurs_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP - INTERVAL '1 year'
);

CREATE TABLE url_checks (
    id SERIAL PRIMARY KEY,
    url_id INTEGER REFERENCES urls (id),
    is_ok BOOLEAN NOT NULL,
    status INTEGER NOT NULL,
    error_message TEXT,
    checked_on TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX url_checks_urls_fkey ON url_checks(url_id);